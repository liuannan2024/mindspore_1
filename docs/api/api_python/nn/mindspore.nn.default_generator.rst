mindspore.nn.default_generator
==============================

.. py:function:: mindspore.nn.default_generator()

    默认的生成器对象。

    当用户没有指定生成器时，随机算子会调用默认生成器来生成随机数。

    返回：
        全局默认生成器。

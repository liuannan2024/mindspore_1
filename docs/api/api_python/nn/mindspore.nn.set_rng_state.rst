mindspore.nn.set_rng_state
==========================

.. py:function:: mindspore.nn.set_rng_state(seed, offset=None)

    设置默认生成器状态。

    参数：
        - **seed** (int) - 默认生成器的种子。
        - **offset** (int，可选) - 默认生成器的偏移量，默认值是：``None`` ，取 ``0`` 。
